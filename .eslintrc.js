module.exports = {
    parser: 'babel-eslint',
    env: {
        commonjs: true,
        browser: true,
        node: true,
    },
    extends: ['airbnb-base','prettier'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaVersion: 2018,
    },
    plugins: ['prettier'],
    rules: {
        'prettier/prettier': 'error',
    },
};