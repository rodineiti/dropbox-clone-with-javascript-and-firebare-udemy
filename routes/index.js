const express = require('express');

const router = express.Router();
const formidable = require('formidable');
const fs = require('fs');

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

router.get('/file', (req, res) => {
  const path = `./${req.query.path}`;

  if (fs.existsSync(path)) {
    fs.readFile(path, (error, data) => {
      if (error) {
        console.error(error);
        res.status(404).json({
          error,
        });
      } else {
        res.status(200).end(data);
      }
    });
  } else {
    res.status(404).json({
      error: 'File not found',
    });
  }
});

router.post('/upload', (req, res) => {
  const form = new formidable.IncomingForm({
    uploadDir: './uploads',
    keepExtensions: true,
  });

  form.parse(req, (error, fields, files) => {
    res.json({
      files,
    });
  });
});

router.delete('/file', (req, res) => {
  const form = new formidable.IncomingForm({
    uploadDir: './uploads',
    keepExtensions: true,
  });

  form.parse(req, (error, fields, files) => {
    const path = `./${fields.path}`;

    if (fs.existsSync(path)) {
      fs.unlink(path, (error) => {
        if (error) {
          res.status(400).json({
            error,
          });
        } else {
          res.json({
            fields,
          });
        }
      });
    } else {
      res.status(404).json({
        error: 'File not found',
      });
    }
  });
});

module.exports = router;
